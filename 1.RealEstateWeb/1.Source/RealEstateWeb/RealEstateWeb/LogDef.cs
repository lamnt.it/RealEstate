﻿/************************************************************************/
/* All Rights Reserved. Copyright (C) AIOSolutions                      */
/************************************************************************/
/* File Name    : LogDef.cs                                             */
/* Function     : Log                                                   */
/* System Name  : Real Estate Website                                   */
/* Create       : LoanDinh 2020/07/17                                   */
/* Comment      :                                                       */
/************************************************************************/

namespace AIOSolutions.RealEstateWeb.Web
{
    using System;
    using System.Text;

    public class LogDef
    {
        /// <summary>
        /// Log Infor
        /// [ControllerName ActionName LogInfor]
        /// </summary>
        private static string logMsg = "{0} {1} {2}";

        public static readonly string StartLogMsg = "Start";

        /// <summary>
        /// Get Log Infor
        /// </summary>
        public static string GetLogInfo(Object obj1, Object obj2, Object obj3)
        {
            var sb = new StringBuilder();
            sb.AppendFormat(logMsg, obj1, obj2, obj3);
            return sb.ToString();
        }
    }
}
