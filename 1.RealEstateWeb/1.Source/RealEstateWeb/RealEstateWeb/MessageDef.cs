﻿/************************************************************************/
/* All Rights Reserved. Copyright (C) AIOSolutions                      */
/************************************************************************/
/* File Name    : MessageDef.cs                                         */
/* Function     : Message                                               */
/* System Name  : Real Estate Website                                   */
/* Create       : LoanDinh 2020/07/17                                   */
/* Comment      :                                                       */
/************************************************************************/

namespace AIOSolutions.RealEstateWeb.Web
{
    using System.Collections.Generic;

    /// <summary>
    /// defind messageID list
    /// </summary>
    public enum MESSAGE_ID
    {

    }

    /// <summary>
    /// Message Class
    /// </summary>
    public class MessageDef
    {
        /// <summary>
        /// defind message infor
        /// </summary>
        private static Dictionary<MESSAGE_ID, string> msgEntry = new Dictionary<MESSAGE_ID, string>()
        {

        };

        /// <summary>
        /// Get message infor
        /// </summary>
        /// <param name="id"> messageID </param>
        /// <returns> message infor </returns>
        public static string GetMessage(MESSAGE_ID id)
        {
            // message infor
            var content = string.Empty;
            return msgEntry.TryGetValue(id, out content) ? content : string.Empty;
        }
    }
}
