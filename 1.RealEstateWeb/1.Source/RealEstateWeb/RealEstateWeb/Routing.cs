﻿/************************************************************************/
/* All Rights Reserved. Copyright (C) AIOSolutions                      */
/************************************************************************/
/* File Name    : Routing.cs                                            */
/* Function     : Routing page                                          */
/* System Name  : Real Estate Website                                   */
/* Create       : LoanDinh 2020/07/17                                   */
/* Comment      :                                                       */
/************************************************************************/

namespace AIOSolutions.RealEstateWeb.Web
{
    public class Routing
    {
        public static readonly string HomeController = "default";
        public static readonly string HomeUrl = "{controller=Home}/{action=Index}/{id?}";

        // admin master page
        public static readonly string AdminController = "admin";
        public static readonly string AdminUrl = "{controller=Admin}/{action=Admin}/{id?}";

        // project detail master page
        public static readonly string ProjectDetailController = "projectdetail";
        public static readonly string ProjectDetailUrl = "{controller=ProjectDetail}/{action=Index}/{id?}";
    }
}
