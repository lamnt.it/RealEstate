﻿/************************************************************************/
/* All Rights Reserved. Copyright (C) AIOSolutions                      */
/************************************************************************/
/* File Name    : SessionDef.cs                                         */
/* Function     : Define Session                                        */
/* System Name  : Real Estate Website                                   */
/* Create       : LoanDinh 2020/07/17                                   */
/* Comment      :                                                       */
/************************************************************************/

namespace AIOSolutions.RealEstateWeb.Web
{
    using Microsoft.AspNetCore.Http;

    using Newtonsoft.Json;

    public static class SessionDef
    {
        /// <summary>
        /// Set sesion
        /// </summary>
        /// <param name="session"> sesion name </param>
        /// <param name="key"> key </param>
        /// <param name="value"> value </param>
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        /// <summary>
        /// Get sesion value
        /// </summary>
        /// <param name="session"> sesion name </param>
        /// <param name="key"> key </param>
        /// <returns> sesion value </returns>
        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
