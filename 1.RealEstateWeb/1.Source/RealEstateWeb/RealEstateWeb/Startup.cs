/************************************************************************/
/* All Rights Reserved. Copyright (C) AIOSolutions                      */
/************************************************************************/
/* File Name    : Startup.cs                                            */
/* Function     : Setup Startup                                         */
/* System Name  : Real Estate Website                                   */
/* Create       : LoanDinh 2020/07/17                                   */
/* Comment      :                                                       */
/************************************************************************/

namespace AIOSolutions.RealEstateWeb.Web
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using AIOSolutions.RealEstateWeb.Repositpries;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Localization;
    using Microsoft.AspNetCore.Localization.Routing;
    using Microsoft.AspNetCore.Rewrite;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.Extensions.Options;

    /// <summary>
    /// Startup Class
    /// </summary>
    public class Startup
    {
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Starting value Setup
        /// </summary>
        /// <param name="configuration"></param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSession(options =>
            {
                options.Cookie.Name = ".RealEstate.Security.Cookie";
                options.IdleTimeout = TimeSpan.FromMinutes(Configuration.GetValue<double>("Session:TimeOut"));
                options.Cookie.IsEssential = true;
            });

            services.AddLocalization(o => { o.ResourcesPath = "Resources"; });
            services.Configure<RequestLocalizationOptions>(
                options =>
                {
                    IList<CultureInfo> supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en-US"),
                        new CultureInfo("vi-VN"),
                    };
                    options.DefaultRequestCulture = new RequestCulture("vi-VN", "vi-VN");
                    options.SupportedCultures = supportedCultures;
                    options.SupportedUICultures = supportedCultures;
                });
            services.AddControllersWithViews().AddNewtonsoftJson(); ;
            services.AddDbContext<DataContext>(option => { option.UseSqlite(Configuration.GetConnectionString("RealEstateDB")); });
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseRequestLocalization();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();

            app.UseRouting();
            app.UseAuthorization();

            app.UseRewriter(new RewriteOptions().Add(new SubdomainRoute()));


            var localizeOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(localizeOptions.Value);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(name: Routing.HomeController, pattern: Routing.HomeUrl);
                endpoints.MapControllerRoute(name: Routing.AdminController, pattern: Routing.AdminUrl);
                endpoints.MapControllerRoute(name: Routing.ProjectDetailController, pattern: Routing.ProjectDetailUrl);
            });

        }
    }
}
