﻿/************************************************************************/
/* All Rights Reserved. Copyright (C) AIOSolutions                      */
/************************************************************************/
/* File Name    : DataContext.cs                                        */
/* Function     : Database Instant Class                                */
/* System Name  : Real Estate Website                                   */
/* Create       : LoanDinh 2020/07/17                                   */
/* Comment      :                                                       */
/************************************************************************/

namespace AIOSolutions.RealEstateWeb.Repositpries
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// DataContext Class
    /// </summary>
    public class DataContext : DbContext
    {
        /// <summary>
        /// Connect Database
        /// </summary>
        /// <param name="optionsBuilder"> Connect option </param>
        public DataContext(DbContextOptions<DataContext> options)
        : base(options)
        {
            Database.EnsureCreated();
        }

        /// <summary>
        /// Create Table's PK (When the table have more than 1PK)
        /// </summary>
        /// <param name="modelBuilder"> ModelBuilder </param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Ex:
            // modelBuilder.Entity<HenkanRireki>().HasKey(henkanRireki => new { henkanRireki.UserId, henkanRireki.ConvertTime });
        }

        // Declare database table Object
        // Ex:
        // public DbSet<HenkanRireki> HenkanRireki { get; set; }
    }
}
